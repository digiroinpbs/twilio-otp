package main

import (
	"net/http"
	"fmt"
	"io/ioutil"
	"github.com/buger/jsonparser"
	"encoding/json"
	"net/url"
	"strings"
	"time"
	"math/rand"
	"strconv"
	"github.com/go-redis/redis"
	"github.com/magiconair/properties"
)

func initRedis() *redis.Client{
	p := properties.MustLoadFile("/etc/service.conf", properties.UTF8)
	address := p.GetString("redis.name","localhost")+":"+p.GetString("redis.port","6379")
	client := redis.NewClient(&redis.Options{
		Addr:     address,
		Password: p.GetString("redis.password",""),
		DB:       p.GetInt("redis.db",0),
	})
	return client
}

func getParameter()map[string]string{
	p := properties.MustLoadFile("/etc/service.conf", properties.UTF8)
	username:=p.GetString("twilio.sid","AC8e0e5aa72d449f96747198b3aaa4da44")
	password:=p.GetString("twilio.key","0e978a864b0a468c9f94111f6b4a07de")
	timeout:=p.GetString("twilio.timeout","5")
	from:=p.GetString("twilio.from","6285574679971")
	return map[string]string{
		"Username": username,
		"Password": password,
		"Timeout": timeout,
		"From": from,
	}
}

type Otp struct {
	PhoneNumber string
	Message string
}

type SendOtp struct {
	PhoneNumber string
	OTP string
}

func main() {
	http.HandleFunc("/digiroin/webhook", balance)
	http.HandleFunc("/digiroin/sendOtp", sendOtp)
	http.HandleFunc("/digiroin/checkOtp", checkOtp)
	http.ListenAndServe(":7043", nil)
}

func balance(w http.ResponseWriter, r *http.Request) {
	body, _ := ioutil.ReadAll(r.Body)
	code, _:= jsonparser.ParseString(body)
	fmt.Println(code)
	fmt.Println("masuk")
}

//contoh input
//{
//"PhoneNumber":"6281213631232",
//"Message":""
//}
func sendOtp(w http.ResponseWriter, r *http.Request) {
	req := Otp{}
	client := &http.Client{}
	redis := initRedis()
	param := getParameter()
	err := json.NewDecoder(r.Body).Decode(&req)
	result :=""
	if(err!=nil){
		w.WriteHeader(http.StatusBadRequest)
		result = `{"Error":"`+err.Error()+`"}`
		w.Write([]byte(result))
	}else{
		form := url.Values{}
		form.Add("From", param["From"])
		form.Add("To",req.PhoneNumber)
		rand :=strconv.Itoa(random())
		form.Add("Body",req.Message+rand)
		timeout,_ := strconv.Atoi(param["Timeout"])
		redis.Set("otp:"+req.PhoneNumber,rand,time.Duration(time.Minute*time.Duration(timeout)))
		req, err := http.NewRequest("POST", "https://api.twilio.com/2010-04-01/Accounts/AC8e0e5aa72d449f96747198b3aaa4da44/Messages.json", strings.NewReader(form.Encode()))
		req.SetBasicAuth(param["Username"],param["Password"])
		req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
		resp, err := client.Do(req)
		if(err==nil){
			rs ,_:=ioutil.ReadAll(resp.Body)
			w.Write([]byte(rs))
		}else{
			w.WriteHeader(http.StatusInternalServerError)
			result = `{"Error":"`+err.Error()+`"}`
			w.Write([]byte(result))
		}
	}
}

//contoh input
//{
//"PhoneNumber":"6281213631232",
//"OTP":""
//}
func checkOtp(w http.ResponseWriter, r *http.Request) {
	req := SendOtp{}
	redis := initRedis()
	err := json.NewDecoder(r.Body).Decode(&req)
	result :=""
	if(err!=nil){
		w.WriteHeader(http.StatusBadRequest)
		result = `{"Error":"`+err.Error()+`"}`
		w.Write([]byte(result))
	}else{
		res := redis.Get("otp:"+req.PhoneNumber)
		if(res.Val()==""){
			w.WriteHeader(http.StatusBadRequest)
			result=`{"status":"send otp first"}`
			w.Write([]byte(result))
		} else if(res.Val()==req.OTP){
			result=`{"status":"success"}`
			w.Write([]byte(result))
			redis.Del("otp:"+req.PhoneNumber)
		}else{
			w.WriteHeader(http.StatusNotFound)
			result=`{"status":"failed"}`
			w.Write([]byte(result))
		}
	}
}

func random() int{
	rand.Seed(time.Now().Unix())
	val := int(rand.Intn(999999 - 100000) + 100000)
	return val
}